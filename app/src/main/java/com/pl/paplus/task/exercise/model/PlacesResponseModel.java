package com.pl.paplus.task.exercise.model;

/**
 * Created by tomek on 19.01.15.
 */
public class PlacesResponseModel {
    private Meta meta;

    public FoursquareResponse getResponse() {
        return response;
    }

    public void setResponse(FoursquareResponse response) {
        this.response = response;
    }

    private FoursquareResponse response;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
