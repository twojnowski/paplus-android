package com.pl.paplus.task.exercise.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by tomek on 20.01.15.
 */
public class UserLocationProvider {

    private static final String LAT_KEY = "lat_key";
    private static final String LNG_KEY = "lng_key";
    private static final String LOCATION_PREFS_NAME = "UserLocationProvider_SharedPreferences";
    private static final String EMPTY_VAL = "";

    public static void saveUserLocation(Context context, double lat, double lng){
        SharedPreferences sharedPref = context.getSharedPreferences(LOCATION_PREFS_NAME, Context.MODE_PRIVATE);
        sharedPref.edit().putString(LAT_KEY,Double.toString(lat)).putString(LNG_KEY,Double.toString(lng)).commit();
    }

    public static LatLng getUserLocation(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(LOCATION_PREFS_NAME, Context.MODE_PRIVATE);
        String lat = sharedPref.getString(LAT_KEY, EMPTY_VAL);
        String lng = sharedPref.getString(LNG_KEY, EMPTY_VAL);
        if(TextUtils.isEmpty(lat) || TextUtils.isEmpty(lng))
            return DefaultLocationProvider.getDefaultLocation();
        double dlat = Double.parseDouble(lat);
        double dlng = Double.parseDouble(lng);
        Log.i("LAT LNG",dlat+","+dlng);
        return new LatLng(dlat,dlng);
    }

    public static String getStringUserLocation(Context context){
        LatLng latLng = getUserLocation(context);
        return latLng.latitude+","+latLng.longitude;
    }

}
