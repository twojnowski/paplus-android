package com.pl.paplus.task.exercise.model;

import java.util.List;

/**
 * Created by tomek on 20.01.15.
 */
public class FoursquareItemPhotos {
    private static final int EMPTY_LIST_VALUE = 0;
    private static final int FIRST_INDEX = EMPTY_LIST_VALUE;
    private List<FoursquareItemPhotoGroup> groups;

    public List<FoursquareItemPhotoGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<FoursquareItemPhotoGroup> groups) {
        this.groups = groups;
    }

    public FoursquareItemPhotoGroup getFirstFoursquareItemPhotoGroup(){
        if(getGroups() == null || getGroups().size() == EMPTY_LIST_VALUE)
            return null;
        return getGroups().get(FIRST_INDEX);
    }

}
