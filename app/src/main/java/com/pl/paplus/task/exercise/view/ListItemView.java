package com.pl.paplus.task.exercise.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.pl.paplus.task.exercise.R;
import com.pl.paplus.task.exercise.model.FoursquareItem;
import com.pl.paplus.task.exercise.network.VolleyHelper;

/**
 * Created by tomek on 19.01.15.
 */
public class ListItemView extends RelativeLayout {

    private TextView titleView;
    private NetworkImageView imageView;

    public void bindData(FoursquareItem foursquareItem){
        titleView.setText(foursquareItem.getVenue().getName());
        imageView.setImageUrl(foursquareItem.getVenue().getFirstPhotoPath(), VolleyHelper.getInstance(getContext()).getImageLoader());
    }

    private void initViews(){
        titleView = (TextView) findViewById(R.id.textView);
        imageView = (NetworkImageView) findViewById(R.id.imageView);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initViews();
    }

    public static ListItemView inflate(ViewGroup parent) {
        ListItemView itemView = (ListItemView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_layout, parent, false);
        return itemView;
    }

    public ListItemView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ListItemView(Context context) {
        this(context,null);
    }

}
