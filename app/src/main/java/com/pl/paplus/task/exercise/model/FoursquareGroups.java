package com.pl.paplus.task.exercise.model;

import java.util.List;

/**
 * Created by tomek on 19.01.15.
 */
public class FoursquareGroups {
    private List<FoursquareItem> items;

    public List<FoursquareItem> getItems() {
        return items;
    }

    public void setItems(List<FoursquareItem> items) {
        this.items = items;
    }
}
