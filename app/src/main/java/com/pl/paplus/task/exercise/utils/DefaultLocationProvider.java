package com.pl.paplus.task.exercise.utils;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by tomek on 20.01.15.
 */
public class DefaultLocationProvider {

    private static final double LAT_LODZ = 51.755907;
    private static final double LNG_LODZ = 19.455358;

    public static LatLng getDefaultLocation(){
        return new LatLng(LAT_LODZ,LNG_LODZ);
    }

}
