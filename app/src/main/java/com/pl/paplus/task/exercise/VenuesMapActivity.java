package com.pl.paplus.task.exercise;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pl.paplus.task.exercise.model.FoursquareItem;
import com.pl.paplus.task.exercise.model.Meta;
import com.pl.paplus.task.exercise.model.PlacesResponseModel;
import com.pl.paplus.task.exercise.network.FoursquareRequest;
import com.pl.paplus.task.exercise.network.VolleyHelper;
import com.pl.paplus.task.exercise.utils.DefaultLocationProvider;
import com.pl.paplus.task.exercise.utils.DialogHelper;
import com.pl.paplus.task.exercise.utils.UserLocationProvider;

import java.util.List;


public class VenuesMapActivity extends Activity implements OnMapReadyCallback,Response.Listener<PlacesResponseModel>,Response.ErrorListener{

    private static final float ZOOM_LEVEL = 11;
    private ProgressDialog progress;
    private List<FoursquareItem> foursquareItems;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venues_map);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
        googleMap = mapFragment.getMap();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        if(foursquareItems == null){
            showProgress();
            makeFoursquareRequest();
        }
    }

    private void handleResponse(){
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(UserLocationProvider.getUserLocation(this), ZOOM_LEVEL));


        for(FoursquareItem foursquareItem : foursquareItems){
            googleMap.addMarker(new MarkerOptions()
                    .title(foursquareItem.getVenue().getName())
                    .position(new LatLng(
                            foursquareItem.getVenue().getLocation().getLat(),
                            foursquareItem.getVenue().getLocation().getLng()
                    )));
        }
    }

    private void makeFoursquareRequest(){
        VolleyHelper.getInstance(this).addToRequestQueue(new FoursquareRequest(this,this,this));
    }

    private void showProgress(){
        progress = ProgressDialog.show(this, getString(R.string.info),getString(R.string.loading), true);
    }

    private void hideProgress(){
        if(progress != null)
            progress.dismiss();
    }

    private void handleError(){
        hideProgress();
        DialogHelper.showDialog(getString(R.string.error_msg), this);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        handleError();
    }

    @Override
    public void onResponse(PlacesResponseModel response) {
        if(response.getMeta().getCode() == Meta.VALID_RESPONSE_CODE){
            this.foursquareItems = response.getResponse().getFirstItems();
            handleResponse();
            hideProgress();
        }else{
            handleError();
        }
    }

}
