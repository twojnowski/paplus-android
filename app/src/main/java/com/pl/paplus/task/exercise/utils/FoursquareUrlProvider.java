package com.pl.paplus.task.exercise.utils;

import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tomek on 19.01.15.
 */
public class FoursquareUrlProvider {


    private static final String SERVER_URL = "https://api.foursquare.com/v2/venues/explore?";
    private static final String CLIENT_ID = "40ZZDMX0KFMQTBGQ1LDNRKKWX40J1CCR1MFJC0IUW55DY2YB";
    private static final String CLIENT_SECRET = "3QTYROSHN25KMNJYP1MV1AUJWA5FDFE5Q5GHL4JCIS425WI1";
    //private static final String PLACE_LOCATION = "Lodz,Pl";
    private static final String PLACE_SECTION_FILTER = "food";

    public static final String getPlacesForUserLocation(Context context){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(SERVER_URL);
        stringBuilder.append("ll="+UserLocationProvider.getStringUserLocation(context));
        stringBuilder.append("&section="+PLACE_SECTION_FILTER);
        stringBuilder.append("&venuePhotos=1");
        stringBuilder.append("&client_id="+CLIENT_ID);
        stringBuilder.append("&client_secret="+CLIENT_SECRET);
        stringBuilder.append("&v="+getFormattedNowDate());
        return stringBuilder.toString();
    }

    private static final String getFormattedNowDate(){
        Date now = new Date();
        String formattedDate = new SimpleDateFormat("yyyyMMdd").format(now);
        return formattedDate;
    }

}
