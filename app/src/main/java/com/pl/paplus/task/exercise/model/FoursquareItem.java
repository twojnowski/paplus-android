package com.pl.paplus.task.exercise.model;

import java.util.List;

/**
 * Created by tomek on 19.01.15.
 */
public class FoursquareItem {
    private static final int EMPTY_LIST_VALUE = 0;
    private VenueItem venue;
    private List<FoursquareItemTip> tips;

    public VenueItem getVenue() {
        return venue;
    }

    public void setVenue(VenueItem venue) {
        this.venue = venue;
    }

    public List<FoursquareItemTip> getTips() {
        return tips;
    }

    public void setTips(List<FoursquareItemTip> tips) {
        this.tips = tips;
    }

    public String getTipsListAsString(){
        if(getTips() == null || getTips().size() == EMPTY_LIST_VALUE){
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for(FoursquareItemTip tip : getTips()){
            stringBuilder.append(tip.getText()+"\n");
        }
        return stringBuilder.toString();
    }

}
