package com.pl.paplus.task.exercise.model;

/**
 * Created by tomek on 19.01.15.
 */
public class FoursquareItemTip {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
