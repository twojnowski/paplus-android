package com.pl.paplus.task.exercise.model;

/**
 * Created by tomek on 20.01.15.
 */
public class FoursquareItemPhoto {
    private static final String SIZE = "30x30";
    private String prefix;
    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getPhotoPath(){
        return prefix+SIZE+suffix;
    }
}
