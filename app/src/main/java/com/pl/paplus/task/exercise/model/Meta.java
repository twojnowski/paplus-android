package com.pl.paplus.task.exercise.model;

/**
 * Created by tomek on 19.01.15.
 */
public class Meta {

    public static final int VALID_RESPONSE_CODE = 200;

    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
