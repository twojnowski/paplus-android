package com.pl.paplus.task.exercise.model;

/**
 * Created by tomek on 20.01.15.
 */
public class VenueLocation {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
