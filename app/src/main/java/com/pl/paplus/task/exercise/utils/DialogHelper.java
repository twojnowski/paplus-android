package com.pl.paplus.task.exercise.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.pl.paplus.task.exercise.R;


/**
 * Created by tomek on 19.01.15.
 */
public class DialogHelper {

    public static void showDialog(String msg,Context context){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(context.getString(R.string.info));

        alertDialog.setMessage(msg);

        alertDialog.setButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }


}
