package com.pl.paplus.task.exercise.network;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;

import com.pl.paplus.task.exercise.model.PlacesResponseModel;
import com.pl.paplus.task.exercise.utils.FoursquareUrlProvider;

/**
 * Created by tomek on 19.01.15.
 */
public class FoursquareRequest extends Request<PlacesResponseModel> {

    private final Response.Listener<PlacesResponseModel> listener;

    public FoursquareRequest(Response.Listener<PlacesResponseModel> listener, Response.ErrorListener errorListener,Context context) {
        super(Request.Method.GET, FoursquareUrlProvider.getPlacesForUserLocation(context), errorListener);
        this.listener = listener;
    }

    @Override
    protected Response<PlacesResponseModel> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(
                    new Gson().fromJson(json, PlacesResponseModel.class),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(PlacesResponseModel arg0) {
        listener.onResponse(arg0);
    }

}
