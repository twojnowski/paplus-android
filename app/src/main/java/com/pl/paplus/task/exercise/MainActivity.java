package com.pl.paplus.task.exercise;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.List;

import com.pl.paplus.task.exercise.model.FoursquareItem;
import com.pl.paplus.task.exercise.model.Meta;
import com.pl.paplus.task.exercise.model.PlacesResponseModel;
import com.pl.paplus.task.exercise.network.FoursquareRequest;
import com.pl.paplus.task.exercise.network.VolleyHelper;
import com.pl.paplus.task.exercise.utils.DialogHelper;
import com.pl.paplus.task.exercise.view.ListViewItemAdapter;

public class MainActivity extends Activity implements Response.Listener<PlacesResponseModel>,Response.ErrorListener,AdapterView.OnItemClickListener{

    private static final String tag = MainActivity.class.getSimpleName();

    private ProgressDialog progress;
    private ListView listView;
    private ListViewItemAdapter listViewItemAdapter;
    private List<FoursquareItem> foursquareItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(foursquareItems == null){
            showProgress();
            makeFoursquareRequest();
        }
    }

    public void showMapActivity(View v){
        Intent intent = new Intent(this,VenuesMapActivity.class);
        startActivity(intent);
    }

    private void makeFoursquareRequest(){
        VolleyHelper.getInstance(this).addToRequestQueue(new FoursquareRequest(this,this,this));
    }

    @Override
    public void onResponse(PlacesResponseModel response) {
        if(response.getMeta().getCode() == Meta.VALID_RESPONSE_CODE){
            foursquareItems =  response.getResponse().getFirstItems();
            listViewItemAdapter = new ListViewItemAdapter(foursquareItems);
            listView.setAdapter(listViewItemAdapter);
            hideProgress();
        }else{
            handleError();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        handleError();
    }

    private void showProgress(){
        progress = ProgressDialog.show(this, getString(R.string.info),getString(R.string.loading), true);
    }

    private void hideProgress(){
        if(progress != null)
            progress.dismiss();
    }

    private void handleError(){
        hideProgress();
        DialogHelper.showDialog(getString(R.string.error_msg), this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FoursquareItem foursquareItem = (FoursquareItem) parent.getItemAtPosition(position);
        String tips = foursquareItem.getTipsListAsString();
        if(TextUtils.isEmpty(tips)){
            DialogHelper.showDialog(getString(R.string.no_tips),this);
        }else{
            DialogHelper.showDialog(tips,this);
        }
    }
}
