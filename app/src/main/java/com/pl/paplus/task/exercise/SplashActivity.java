package com.pl.paplus.task.exercise;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.pl.paplus.task.exercise.utils.LastKnownLocationManager;
import com.pl.paplus.task.exercise.utils.UserLocationProvider;


public class SplashActivity extends Activity implements LastKnownLocationManager.LocationResult {

    private static final String tag = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new LastKnownLocationManager().getLocation(this,this);
    }

    @Override
    public void onLocationRetrieved(Location location) {
        if(location != null){
            UserLocationProvider.saveUserLocation(SplashActivity.this,location.getLatitude(),location.getLongitude());
        }
        showListActivity();
    }

    private void showListActivity(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(LastKnownLocationManager.SLEEP_TIME);
                }catch (Exception e){}
                Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        }).start();
    }

}
