package com.pl.paplus.task.exercise.model;

/**
 * Created by tomek on 19.01.15.
 */
public class VenueItem {
    private String name;
    private FoursquareItemPhotos photos;
    private VenueLocation location;

    public VenueLocation getLocation() {
        return location;
    }

    public void setLocation(VenueLocation location) {
        this.location = location;
    }

    public FoursquareItemPhotos getPhotos() {
        return photos;
    }

    public void setPhotos(FoursquareItemPhotos photos) {
        this.photos = photos;
    }

    public String getFirstPhotoPath(){
        return photos.getFirstFoursquareItemPhotoGroup().getFirstFoursquareItemPhoto().getPhotoPath();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
