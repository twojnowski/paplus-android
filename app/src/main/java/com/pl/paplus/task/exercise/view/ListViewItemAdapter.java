package com.pl.paplus.task.exercise.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import com.pl.paplus.task.exercise.model.FoursquareItem;

/**
 * Created by tomek on 19.01.15.
 */
public class ListViewItemAdapter extends BaseAdapter {

    private List<FoursquareItem> items;

    public ListViewItemAdapter(List<FoursquareItem> items) {
        super();
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public FoursquareItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemView itemView = (ListItemView) convertView;
        if(itemView == null){
            itemView = ListItemView.inflate(parent);
        }
        itemView.bindData(getItem(position));
        return itemView;
    }

}
