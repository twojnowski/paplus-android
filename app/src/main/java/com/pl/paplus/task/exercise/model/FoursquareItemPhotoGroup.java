package com.pl.paplus.task.exercise.model;

import java.util.List;

/**
 * Created by tomek on 20.01.15.
 */
public class FoursquareItemPhotoGroup {
    private static final int EMPTY_LIST_VALUE = 0;
    private static final int FIRST_INDEX = EMPTY_LIST_VALUE;
    private List<FoursquareItemPhoto> items;

    public List<FoursquareItemPhoto> getItems() {
        return items;
    }

    public void setItems(List<FoursquareItemPhoto> items) {
        this.items = items;
    }

    public FoursquareItemPhoto getFirstFoursquareItemPhoto(){
        if(getItems() == null || getItems().size() == EMPTY_LIST_VALUE)
            return null;
        return getItems().get(FIRST_INDEX);
    }

}
