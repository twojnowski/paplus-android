package com.pl.paplus.task.exercise.model;

import java.util.List;

/**
 * Created by tomek on 19.01.15.
 */
public class FoursquareResponse {

    private static final int FIRST_INDEX = 0;

    private List<FoursquareGroups> groups;

    public List<FoursquareGroups> getGroups() {
        return groups;
    }

    public void setGroups(List<FoursquareGroups> groups) {
        this.groups = groups;
    }

    public List<FoursquareItem> getFirstItems(){
        return groups.get(FIRST_INDEX).getItems();
    }

}
